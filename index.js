//bài 1
/*
 - Họ Tên:
 *  - Tổng thu nhập năm
 *  - Số người phụ thuộc
 * output: Xuất ra số tiền thuế
 * progress: tính tổng tiền thu nhập  sau đó nhân với thuế suất
*/
document.getElementById("tinhtienthue").onclick = function () {
    var name = document.getElementById("name").value;
    var thunhap = document.getElementById("thunhap").value * 1;
    var nguoiphuthuoc = document.getElementById("nguoiphuthuoc").value * 1;
    var tong = 0;
    var ketqua = 0;
    tong = thunhap - 4e6 - 16e5 * nguoiphuthuoc;

    if (tong > 0 && tong <= 6e7) {
        ketqua = 0.05 * tong
    }
    else if (tong > 6e7 && tong <= 12e7) {
        ketqua = 0.1 * tong
    }
    else if (tong > 12e7 && tong <= 21e7) {
        ketqua = 0.15 * tong
    }
    else if (tong > 21e7 && tong <= 384e6) {
        ketqua = 0.2 * tong
    }
    else if (tong > 384e6 && tong <= 624e6) {
        ketqua = 0.25 * tong
    }
    else if (tong > 624e6 && tong <= 96e7) {
        ketqua = 0.3 * tong
    }
    else if (tong > 96e7) {
        ketqua = 0.35 * tong
    }
    else {
        alert("Số tiền thu nhập không hợp lệ")
    }
    ketqua = new Intl.NumberFormat("vn-VN").format(ketqua)
    document.getElementById("txttinhtienthue").innerHTML = "Họ tên: " + name + "; Tiền thuế thu nhập cá nhân: " + ketqua + " VND"
}


//bài 2
/*
input:  
 *  - Mã khách hàng
 *  - Loại khách hàng
 *  - so ket noi
 *  - so kenh cao cap
 * output: In ra số tiền Cáp
 * progress:
 * - lấy lựa chọn của user (if== nhà dân ==> ẩn số kn) else ==> hiện số kết nối
 *  if user == nhaDan => soTien = phí sử lý hóa đơn + Phí dịch vụ cơ bản + thuê kênh cao cấp * 7.5
 * else soTien == phi sử lí hóa đơn + phi dic vu co ban (if <= 10 ==> phi dich vu co ban == soketnoi *7.5) (else phi dich vu co ban === 75 + (soketnoi - 10) * 5) + thuê kênh cao cấp * 50

*/
const ketNoi = document.querySelector('#soketnoi')
ketNoi.style.display = 'none';
var khachHang = document.getElementById('chonkhachhang')
khachHang.onchange = function () {
    khachHang.value;
    if (khachHang.value == 'doanhnghiep') {
        ketNoi.style.display = 'block';
    } else {
        ketNoi.style.display = 'none';

    }
}

document.getElementById('tinhtiencap').onclick = function () {
    var chonkhachhang = document.getElementById("chonkhachhang").value;
    var makhachhang = document.getElementById("makhachhang").value * 1;
    var sokenhcaocap = document.getElementById("sokenh").value * 1;
    var soketnoi = document.getElementById("soketnoi").value * 1;
    var giaTienTrenKenh;
    var tienKetNoi;
    var tiencap = 0;

    if (chonkhachhang == "nhandan") {
        giaTienTrenKenh = sokenhcaocap * 7.5;
        tiencap = 25 + giaTienTrenKenh;
        document.getElementById('txttinhtiencap').innerHTML = `<p>Mã Khác Hàng: ${makhachhang}</p>    <p>Phí Xử Lý Hóa Đơn: 4.5$</p>  <p>Phí Dịch Vụ Cơ Bản: 20.5$</p>  <p>Thuê Kênh Cao Cấp: ${giaTienTrenKenh}$</p> <p>Tổng = ${tiencap}$</p>`
    }
    if (chonkhachhang == "doanhnghiep") {
        if (soketnoi <= 10) {
            giaTienTrenKenh = sokenhcaocap * 50;
            tienKetNoi = soketnoi * 7.5;
            tiencap = giaTienTrenKenh + tienKetNoi;
        } else {
            giaTienTrenKenh = sokenhcaocap * 50;
            tienKetNoi = 75 + (soketnoi - 10) * 5;
            tiencap = 15 + giaTienTrenKenh + tienKetNoi;
        }
        document.getElementById('txttinhtiencap').innerHTML = `<p>Mã Khác Hàng: ${makhachhang}</p> <p>Phí Xử Lý Hóa Đơn: 15$</p>  <p>Phí Dịch Vụ Cơ Bản: ${tienKetNoi}$</p>  <p>Thuê Kênh Cao Cấp: ${giaTienTrenKenh}$</p> <p>Tổng = ${tiencap}$</p>`
    }

}





